This app instance is pre-setup with an admin account.

<nosso>

The initial credentials are:

`username: admin`

`password: changeme`

</nosso>

<sso>

This app integrates with Cloudron authentication. Cloudron users can login and use NextCloud.

However, _admin_ status of Cloudron user is not carried over to NextCloud. For this reason,
this app comes with an pre-setup admin user. This admin user can grant admin previleges to
other users.

`username: admin`

`password: changeme`

</sso>

**Please change the admin password on first login**
