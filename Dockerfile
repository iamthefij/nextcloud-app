FROM cloudron/base:0.10.0
MAINTAINER Johannes Zellner <johannes@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && apt-get install -y php-redis php-zip smbclient && rm -r /var/cache/apt /var/lib/apt/lists

ADD nextcloud.asc /root/
RUN gpg --import /root/nextcloud.asc

# get nextcloud source
ENV VERSION 11.0.3
RUN wget https://download.nextcloud.com/server/releases/nextcloud-${VERSION}.tar.bz2 && \
    wget https://download.nextcloud.com/server/releases/nextcloud-${VERSION}.tar.bz2.sha256 && \
    wget https://download.nextcloud.com/server/releases/nextcloud-${VERSION}.tar.bz2.asc && \
    sha256sum --check nextcloud-${VERSION}.tar.bz2.sha256 && \
    gpg --verify nextcloud-${VERSION}.tar.bz2.asc nextcloud-${VERSION}.tar.bz2 && \
    tar -xj --strip-components 1 -f nextcloud-${VERSION}.tar.bz2 && \
    rm nextcloud-${VERSION}.tar.bz2 nextcloud-${VERSION}.tar.bz2.asc nextcloud-${VERSION}.tar.bz2.sha256

# create config folder link to make the config survive updates
RUN rm -rf /app/code/config && ln -s /app/data/config /app/code/config && \
    mv /app/code/apps /app/apps_template && ln -s /app/data/apps /app/code/apps && \
    mv /app/code/.user.ini /app/.user.ini.orig && ln -s /app/data/.user.ini /app/code/.user.ini && \
    mv /app/code/.htaccess /app/.htaccess.orig && ln -s /app/data/.htaccess /app/code/.htaccess

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf
RUN a2disconf other-vhosts-access-log
ADD apache2-nextcloud.conf /etc/apache2/sites-enabled/nextcloud.conf

RUN crudini --set /etc/php/7.0/apache2/php.ini Session session.save_path /run/sessions

ADD start.sh cron.sh /app/

RUN chown -R www-data.www-data /app/code

CMD [ "/app/start.sh" ]
